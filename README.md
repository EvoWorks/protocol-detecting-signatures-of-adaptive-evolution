# Protocol: Detecting signatures of adaptive evolution

This is a repository for the general introdution to the principles of parameter estimation, hypothesis testing, and site classification using codon models published in *Current Protocols in Molecular Biology* (101:19.1.1-19.1.21, January 2013).

The unit includes four activities, each with an explicit analytical protocol (and supporting files), based on programs provided by the Phylogenetic Analysis by Maximum Likelihood (PAML) package.


## CMPB title: Detecting the Signatures of Adaptive Evolution in Protein-Coding Genes

## Abstract
The field of molecular evolution, which includes genome evolution, is devoted to finding variation within and between groups of organisms and explaining the processes respon- sible for generating this variation. Many DNA changes are believed to have little to no functional effect, and a neutral process will best explain their evolution. Thus, a central task is to discover which changes had positive fitness consequences and were subject to Darwinian natural selection during the course of evolution. Due the size and complex- ity of modern molecular datasets, the field has come to rely extensively on statistical modeling techniques to meet this analytical challenge. For DNA sequences that encode proteins, one of the most powerful approaches is to employ a statistical model of codon evolution. This unit provides a general introduction to the practice of modeling codon evolution using the statistical framework of maximum likelihood. Four real-data anal- ysis activities are used to illustrate the principles of parameter estimation, robustness, hypothesis testing, and site classification. Each activity includes an explicit analytical protocol based on programs provided by the Phylogenetic Analysis by Maximum Like- lihood (PAML) package. Curr. Protoc. Mol. Biol. 101:19.1.1-19.1.21, 2013, John Wiley & Sons, Inc.

## Keywords:
molecular evolution, protein evolution, selection pressure, codon models, maximum likelihood